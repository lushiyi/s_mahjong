
var NodeMessage = require("../tars/NodeMessage").NodeMessage;
var TarsStream = require('@tars/stream');
var CmdEnumTars = require("../tars/CmdEnumTars").CmdEnum;
var MahjongStructTars = require("../tars/MahjongStructTars").MahjongStruct;

var events = require('events');
var Data = require("../data/data").Data;
var Logic = require("../logic/logic").Logic;


var Events = {};
module.exports.Events = Events;

var mapMahjongInfo = {};

var emitter = new events.EventEmitter();
emitter.on(CmdEnumTars.SERVER_E.GAMESTART, function (current, msgIn, msgOut) {
    console.log("CmdEnumTars.SERVER_E.GAMESTART");
    // 解析输入数据
    let reqGameStart_t = new MahjongStructTars.ReqGameStart_t.create(new TarsStream.InputStream(msgIn.msgData));
    let reqGameStart = reqGameStart_t.toObject();
    let deskNo = reqGameStart.deskNo;

    mapMahjongInfo[deskNo] = new Data(reqGameStart.rules);

    // 执行动作
    Logic.DoReqGameStart(mapMahjongInfo[deskNo]);

    // 返回数据打包
    let respGameStart_t = new MahjongStructTars.RespGameStart_t();
    respGameStart_t.dataPrivate.readFromObject(mapMahjongInfo[deskNo].dataPrivate.GetObject());
    respGameStart_t.rounds.readFromObject(mapMahjongInfo[deskNo].rounds);
    respGameStart_t.cardWall.cardNum = mapMahjongInfo[deskNo].cardWall.cardList.length;
    respGameStart_t.cardWall.endNum = mapMahjongInfo[deskNo].cardWall.endNum;
    respGameStart_t.cardWall.dealer = mapMahjongInfo[deskNo].cardWall.dealer;
    respGameStart_t.cardWall.dice1 = mapMahjongInfo[deskNo].cardWall.dice1;
    respGameStart_t.cardWall.dice2 = mapMahjongInfo[deskNo].cardWall.dice2;
    msgOut.msgData = respGameStart_t.toBinBuffer();

    // console.log("mapMahjongInfo：", mapMahjongInfo[deskNo]);
    current.sendResponse(0, msgOut);
});

emitter.on(CmdEnumTars.SERVER_E.GAMEACTION, function (current, msgIn, msgOut) {
    console.log("CmdEnumTars.SERVER_E.GAMEACTION");
    // 解析输入数据
    let reqMahjongAction_t = new MahjongStructTars.ReqMahjongAction_t.create(new TarsStream.InputStream(msgIn.msgData));
    let reqMahjongAction = reqMahjongAction_t.toObject();
    console.log(reqMahjongAction);
    let deskNo = reqMahjongAction.deskNo;

    // 执行动作
    let result = Logic.DoReqMahjongAction(mapMahjongInfo[deskNo], reqMahjongAction);

    // 返回数据打包
    let respMahjongAction_t = new MahjongStructTars.RespMahjongAction_t();
    respMahjongAction_t.action = reqMahjongAction.action;
    respMahjongAction_t.deskIdx = reqMahjongAction.deskIdx;
    respMahjongAction_t.card = reqMahjongAction.card;
    respMahjongAction_t.rounds.readFromObject(mapMahjongInfo[deskNo].rounds);
    msgOut.msgData = respMahjongAction_t.toBinBuffer();

    console.log("当前回合状态：", mapMahjongInfo[deskNo].rounds);
    current.sendResponse(result, msgOut);
});

emitter.on(CmdEnumTars.SERVER_E.GAMEFINISH, function (current, msgIn, msgOut) {
    console.log("CmdEnumTars.SERVER_E.GAMEFINISH");
    // // 解析输入数据
    // let reqMahjongAction_t = new MahjongStructTars.ReqMahjongAction_t.create(new TarsStream.InputStream(msgIn.msgData));
    // let reqMahjongAction = reqMahjongAction_t.toObject();


    // // 返回数据打包
    // let respMahjongAction = "";
    // let respMahjongAction_t = new MahjongStructTars.RespMahjongAction_t();
    // respMahjongAction_t.readFromObject(respMahjongAction);
    // msgOut.msgData = respMahjongAction_t.toBinBuffer();

    // console.log("mapMahjongInfo：", mapMahjongInfo[deskNo]);
    current.sendResponse(0, msgOut);
});

const DoMessage = function (current, msgIn, msgOut) {
    // 校验协议版本号

    console.log("receive a message which msgCmd is ", msgIn.nCmd);
    emitter.emit(msgIn.nCmd, current, msgIn, msgOut);
}

const error = function (result) {
    console.log("result.response.error.code: ", result.response.error.code);
    console.log("result.response.error.message: ", result.response.error.message);
}

Events.DoMessage = DoMessage;


