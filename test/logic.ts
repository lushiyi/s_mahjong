import { Logic } from "../logic/logic";
import { Data, RulesValue } from "../data/data";



let mapMahjongInfo = {};
let rules: RulesValue = {
    cardType: 0,
    playerNum: 4,
};
let deskNo = 112233;
mapMahjongInfo[deskNo] = new Data(rules);
Logic.DoReqGameStart(mapMahjongInfo[deskNo]);

console.log("mapMahjongInfo：", mapMahjongInfo[deskNo].rounds);
console.log("mapMahjongInfo：", mapMahjongInfo[deskNo].dataPrivate);
console.log("mapMahjongInfo：", mapMahjongInfo[deskNo].dataPrivate.GetObject());