import { Mahjong } from "../logic/mahjong";
import { CardWallValue, RulesValue, Data } from "../data/data";
import { GAMEFLAG } from "../enum/gameFlag";
import { CARDTYPE } from "../enum/cardType";


let cardwall: CardWallValue = {
    cardList: [1, 2, 3, 4],
    endNum: 0,
    dealer: 1,
    dice1: 0,
    dice2: 0,
}

console.log(Mahjong.DrawCards(cardwall));
console.log(cardwall);
console.log(Mahjong.DrawCards(cardwall, 1, true));
console.log(cardwall);

// 洗牌测试
let wallCardList = Mahjong.GetCardWallList(0);
console.log(wallCardList.length);

// 首轮发牌测试
let rules: RulesValue = {
    cardType: 0,
    playerNum: 4,
};
let data: Data = new Data(rules);
data.cardWall.cardList = Mahjong.GetCardWallList(data.rules.cardType);
// 首轮发牌
Mahjong.LicensingCard(data.cardWall, data.dataPrivate, data.rules);
// 首轮摸牌（更新回合信息）
let cardList: number[] = Mahjong.DrawCards(data.cardWall);
data.rounds.Update(GAMEFLAG.GAME_DISCARD_WAIT, cardList[0], data.cardWall.dealer);


console.log(data.cardWall.cardList.length);
console.log(data.rounds);
console.log(data.dataPrivate);


console.log("=====================胡牌测试=========================");

let handList: number[] = [
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 1,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 1,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 1,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 2,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 3,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 4,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 5,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 6,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 7,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 8,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 9,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 9,
    CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + 9,
];
console.log(handList);
let result = Mahjong.CalculateReadyHand(handList, CARDTYPE.CARD_MUSK);
console.log(result);