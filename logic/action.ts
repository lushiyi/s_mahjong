import { Data, MingValue } from "../data/data";
import { GAMEFLAG } from "../enum/gameFlag";
import { ACTIONTYPE, ACTIONRESULT } from "../enum/actionType";
import { Mahjong } from "./mahjong";
import { CARDTYPE } from "../enum/cardType";


export class Action {
    static mapDoAction: any = {}
    /**
     * 玩家执行游戏动作
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    static DoAction(data: Data, mahjongAction: any) {

    };

    /**
     * 玩家执行出牌动作
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionDiscard(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    };

    /**
     * 玩家执行摸牌动作（放弃上家打出的牌）
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionPass(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    };

    /**
     * 玩家头吃上家的牌
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionChiH(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    };

    /**
     * 玩家身吃上家的牌
     * ======================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionChiM(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    };

    /**
     * 玩家尾吃上家的牌
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionChiE(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    };

    /**
     * 玩家碰其他家出的牌
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionPeng(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    }

    /**
     * 玩家明杠其他家的牌
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionGangM(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    }

    /**
     * 玩家摸牌后，暗杠
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionGangA(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    }

    /**
     * 玩家摸牌后，补杠
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionGangB(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    }

    /**
     * 玩家出牌阶段，听牌
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionTing(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_OK;
    }

    /**
     * 玩家选择胡牌
     * =====================
     * @param data 
     * @param mahjongAction 
     */
    private static _DoActionHued(data: Data, mahjongAction: any) {
        return ACTIONRESULT.ACTION_FINISH;
    }




    static DoActionDiscard(data: Data, mahjongAction: any) {
        // 执行动作 1、更新手牌数据
        data.dataPrivate.UpdateHandList(mahjongAction.deskIdx, data.rounds.card, [mahjongAction.card]);

        // 执行动作 5、更新回合数据
        data.rounds.Update(GAMEFLAG.GAME_DISCARD_OK, mahjongAction.card);

        // 执行动作 end、更新玩家可执行状态
        data.userInfo.UpdateUserDoAction(mahjongAction.deskIdx, mahjongAction.action)
    };

    static DoActionNextRound(data: Data, mahjongAction: any) {
        Object.assign(mahjongAction, data.userInfo.GetUserDoAction(data.rounds.deskIdx, data.rounds.card));
        const deskIdx = mahjongAction.deskIdx;

        // 下一回合（摸牌）
        if (mahjongAction.action == ACTIONTYPE.ACTION_PASS) {
            // 执行动作 2、更新玩家弃牌
            data.dataPublic.AddCardToDiscardList(deskIdx, mahjongAction.card);

            // 执行动作 3、摸牌
            let cardList: number[] = Mahjong.DrawCards(data.cardWall);

            // 执行动作 5、更新回合数据
            data.rounds.Update(GAMEFLAG.GAME_DISCARD_WAIT, cardList[0], deskIdx);
        }
        // 补杠成功（摸牌）
        else if (mahjongAction.action == ACTIONTYPE.ACTION_GANG_B) {
            // 执行动作 1、更新玩家手牌（不需要更新）

            // 执行动作 2、更新玩家鸣牌
            this._UpdatePlayerPublic(data, mahjongAction, []);

            // 执行动作 3、摸牌
            let cardList: number[] = Mahjong.DrawCards(data.cardWall, 1, true);

            // 执行动作 5、更新回合数据
            data.rounds.Update(GAMEFLAG.GAME_DISCARD_WAIT, cardList[0], deskIdx);
        }
        // 明杠（摸牌）
        else if (mahjongAction.action == ACTIONTYPE.ACTION_GANG_M) {
            // 执行动作 1、更新玩家手牌
            this._UpdateHandListInWait(data, mahjongAction);

            // 执行动作 3、摸牌
            let cardList: number[] = Mahjong.DrawCards(data.cardWall, 1, true);

            // 执行动作 5、更新回合数据
            data.rounds.Update(GAMEFLAG.GAME_DISCARD_WAIT, cardList[0], deskIdx);
        }
        else if ([
            ACTIONTYPE.ACTION_PENG,
            ACTIONTYPE.ACTION_CHI_H,
            ACTIONTYPE.ACTION_CHI_M,
            ACTIONTYPE.ACTION_CHI_E
        ].indexOf(mahjongAction.action) != -1) {
            // 执行动作 1、更新玩家手牌
            this._UpdateHandListInWait(data, mahjongAction);

            // 执行动作 5、更新回合数据
            data.rounds.Update(GAMEFLAG.GAME_DISCARD_WAIT, CARDTYPE.CARD_MUSK, deskIdx);
        }

        // 执行动作 计算听牌
        data.userInfo.UpdateUserInfoReadyHand(Mahjong.CalculateReadyHand, data.rounds.card,
            data.dataPrivate.GetHandList(deskIdx), deskIdx);

        // 执行动作 end 更新玩家可执行状态
        data.userInfo.InitUserAction();

    };

    static DoActionGangB(data: Data, mahjongAction: any) {
        const deskIdx = mahjongAction.deskIdx;
        // 执行动作 1、更新玩家手牌
        data.dataPrivate.UpdateHandList(deskIdx, data.rounds.card, [mahjongAction.card]);

        // 执行动作 5、更新回合数据
        data.rounds.Update(GAMEFLAG.GAME_GANG_BU, mahjongAction.card, deskIdx);

        // 执行动作 计算听牌
        data.userInfo.UpdateUserInfoReadyHand(Mahjong.CalculateReadyHand, data.rounds.card,
            data.dataPrivate.GetHandList(deskIdx), deskIdx);

        // 执行动作 end、更新玩家可执行状态
        data.userInfo.UpdateUserDoAction(mahjongAction.deskIdx, mahjongAction.action);
    };


    static DoActionGangA(data: Data, mahjongAction: any) {
        const deskIdx = mahjongAction.deskIdx;
        const card = mahjongAction.card;
        const TmpCardList = [card, card, card, card];
        // 执行动作 1、更新手牌数据
        data.dataPrivate.UpdateHandList(mahjongAction.deskIdx, data.rounds.card, TmpCardList);

        // 执行动作 2、更新玩家鸣牌
        this._UpdatePlayerPublic(data, mahjongAction, TmpCardList);

        // 执行动作 3、摸牌
        let cardList: number[] = Mahjong.DrawCards(data.cardWall, 1, true);

        // 执行动作 5、更新回合数据
        data.rounds.Update(GAMEFLAG.GAME_DISCARD_WAIT, cardList[0], mahjongAction.deskIdx);

        // 执行动作 计算听牌
        data.userInfo.UpdateUserInfoReadyHand(Mahjong.CalculateReadyHand, data.rounds.card,
            data.dataPrivate.GetHandList(deskIdx), deskIdx);

        // 执行动作 end 更新玩家可执行状态
        data.userInfo.InitUserAction();
    };

    private static _UpdateHandListInWait(data: Data, mahjongAction: any) {
        const card: number = mahjongAction.card;
        let cardList: number[] = [];
        if (mahjongAction.action == ACTIONTYPE.ACTION_CHI_H) {
            cardList = [card + 1, card + 2];
            data.dataPrivate.UpdateHandList(mahjongAction.deskIdx, CARDTYPE.CARD_MUSK, cardList);
        }
        else if (mahjongAction.action == ACTIONTYPE.ACTION_CHI_M) {
            cardList = [card + 1, card - 1]
            data.dataPrivate.UpdateHandList(mahjongAction.deskIdx, CARDTYPE.CARD_MUSK, cardList);
        }
        else if (mahjongAction.action == ACTIONTYPE.ACTION_CHI_E) {
            cardList = [card - 1, card - 2]
            data.dataPrivate.UpdateHandList(mahjongAction.deskIdx, CARDTYPE.CARD_MUSK, cardList);
        }
        else if (mahjongAction.action == ACTIONTYPE.ACTION_PENG) {
            cardList = [card, card]
            data.dataPrivate.UpdateHandList(mahjongAction.deskIdx, CARDTYPE.CARD_MUSK, cardList);
        }
        else if (mahjongAction.action == ACTIONTYPE.ACTION_GANG_M) {
            cardList = [card, card, card]
            data.dataPrivate.UpdateHandList(mahjongAction.deskIdx, CARDTYPE.CARD_MUSK, cardList);
        }

        this._UpdatePlayerPublic(data, mahjongAction, cardList);
    };

    private static _UpdatePlayerPublic(data: Data, mahjongAction: any, cardList: number[]) {
        let mingValue: MingValue = {
            card: mahjongAction.card,
            action: mahjongAction.action,
            from: data.rounds.deskIdx,
            cardList: cardList,
        };

        if (mahjongAction.action == ACTIONTYPE.ACTION_GANG_B) {
            data.dataPublic.UpdatePengToGangB(mahjongAction.deskIdx, mahjongAction.card);
        } else {
            data.dataPublic.AddMingToMingList(mahjongAction.deskIdx, mingValue);
        }
    };
}