/**
 * 麻将逻辑模块
 * 
 */
import { CARDTYPE, TYPE_WIN, TYPE_READY } from "../enum/cardType";
import { GAMETYPE } from "../enum/gameType";
import * as _ from "lodash";
import { CardWallValue, DataPrivate, RulesValue, ResultValue } from "../data/data";
import { GetOperatorList } from "../logic/common";

export class Mahjong {
    /**
     * 合成牌，并洗牌，返回牌列表
     * ===============
     * @param gameType 游戏类型
     */
    static GetCardWallList(gameType: number) {
        let cardWallList: number[] = [];
        for (let i = 0; i < 4; i++) {  // N * 4
            // 1-9 序数牌
            for (let j = 1; j <= 9; j++) {
                cardWallList.push(CARDTYPE.CARD_NUMB | CARDTYPE.TILE_WAN + j); // 万
                cardWallList.push(CARDTYPE.CARD_NUMB | CARDTYPE.TILE_SUO + j); // 索
                cardWallList.push(CARDTYPE.CARD_NUMB | CARDTYPE.TILE_BIN + j); // 饼
            }

            // 风牌 东南西北
            if ((GAMETYPE.GAME_WIN & gameType) == GAMETYPE.GAME_WIN) {
                for (let j = 1; j <= 4; j++) {
                    cardWallList.push(CARDTYPE.CARD_CHAR | CARDTYPE.TILE_WIN + j);
                }
            }

            // 箭牌 中发白
            if ((GAMETYPE.GAME_ARR & gameType) == GAMETYPE.GAME_ARR) {
                for (let j = 1; j <= 3; j++) {
                    cardWallList.push(CARDTYPE.CARD_CHAR | CARDTYPE.TILE_ARR + j);
                }
            } // 或只有红中
            else if ((GAMETYPE.GAME_ZHO & gameType) == GAMETYPE.GAME_ZHO) {
                cardWallList.push(CARDTYPE.CARD_CHAR | CARDTYPE.TILE_ARR + 1); // 红中
            }
        }

        // 花牌 春夏秋冬梅兰竹菊 8 * 1
        if ((GAMETYPE.GAME_FLOW & gameType) == GAMETYPE.GAME_FLOW) {
            for (let j = 1; j <= 8; j++) {
                cardWallList.push(CARDTYPE.CARD_FLOW + j);
            }
        }

        return _.shuffle(cardWallList);
    };

    /**
     * 玩家从牌墙摸牌
     * ====================
     * 从牌墙中摸出N张牌，支持反向摸牌。
     * @param cardWall 牌墙结构体
     * @param num 摸牌数量？默认1个
     * @param bTail 补牌？默认false
     */
    static DrawCards(cardWall: CardWallValue, num: number = 1, bTail: boolean = false) {
        let cardList: number[] = [];

        if (bTail) {
            cardWall.endNum += num;
            for (let i = 0; i < num; i++) {
                cardList.push(cardWall.cardList.pop());
            }
        } else {
            for (let i = 0; i < num; i++) {
                cardList.push(cardWall.cardList.shift());
            }
        }
        return cardList;
    };

    /**
     * 给玩家发手牌，目前只支持13张手牌
     * =========================
     * @param cardWall 牌墙列表
     * @param DataPrivate 玩家私有数据
     * @param rules 玩法规则
     */
    static LicensingCard(cardWall: CardWallValue, dataPrivate: DataPrivate, rules: RulesValue) {
        let operatorList: number[] = GetOperatorList(cardWall.dealer, rules.playerNum);

        // 0,1,2 每人摸四张牌，3 每人摸一张牌 发4轮牌
        for (let i: number = 0; i < 4; i++) {
            for (let idx of operatorList) {
                if (i == 3) {
                    dataPrivate.AddCardToHandList(idx, this.DrawCards(cardWall));
                } else {
                    dataPrivate.AddCardToHandList(idx, this.DrawCards(cardWall, 4));
                }
            }
        }
    };

    /**
     * 听牌计算
     * ====================
     * 注：外层还需配合鸣牌判断，任意牌数量不能超过四个
     * @param handList 手牌
     * @param card rounds.card
     */
    static CalculateReadyHand(handList: number[], card: number) {
        const copyHandList = _.concat(handList, []);
        if (card != CARDTYPE.CARD_MUSK) {
            copyHandList.push(card);
        }

        const result: ResultValue = {
            readyList: [],
            winMap: {},
        };
        const cardNumber = copyHandList.length;
        if (TYPE_WIN.indexOf(cardNumber) != -1) {
            const tmpHandList = _.concat(copyHandList);
            const winList: number[] = this._CalculateReadyHand(tmpHandList);
            if (winList.length > 0) {
                result.readyList.push(CARDTYPE.CARD_MUSK);
                result.winMap[CARDTYPE.CARD_MUSK] = winList;
            }
        }
        else if (TYPE_READY.indexOf(cardNumber) != -1) {
            const cardMap = _.countBy(copyHandList);
            for (const key in cardMap) {
                if (cardMap.hasOwnProperty(key)) {
                    const tmpHandList = _.concat(copyHandList);
                    // 删除一个card
                    tmpHandList.splice(tmpHandList.indexOf(parseInt(key)), 1)
                    const winList: number[] = this._CalculateReadyHand(tmpHandList);
                    if (winList.length > 0) {
                        result.readyList.push(parseInt(key));
                        result.winMap[key] = winList;
                    }
                }
            }
        }
        // 大小相公
        else {
            console.log("It's impossible to get into this branch !!!");
        }
        return result;
    };

    private static _CalculateReadyHand(tmpHandList: number[]): number[] {
        const winList: number[] = [];
        const cardMap = _.countBy(tmpHandList);

        for (const key in cardMap) {
            const tmpList: number[] = _.concat(tmpHandList);
            console.log("key:", key);
            if (cardMap.hasOwnProperty(key)) {
                // 补将 删除将牌*1
                if (cardMap[key] == 1) {
                    tmpList.splice(tmpList.indexOf(parseInt(key)), 1);
                    // 初选
                    console.log("tmpList:", tmpList);
                    const tmpCardTypeMap = this._GetCardTypeMap(tmpList);
                    console.log("++++++++++++++++++++++++++++");
                    console.log(tmpCardTypeMap);
                    let tmpCardTypeList = _.filter(tmpCardTypeMap, function (o) { return (o.length % 3) != 0; });
                    if (tmpCardTypeList.length != 0) {
                        continue;
                    }
                    console.log("++++++++++初测通过++++++++++");
                    // 计算万索饼 是否AAA或ABC



                    // 成功
                    // winList.push(parseInt(key));
                }
                // 做将 删除将牌*2 (字牌 刻子不能拆做将)
                // else if (!((cardMap[key] != 2) &&
                //     ((parseInt(key) & CARDTYPE.CARD_CHAR) == CARDTYPE.CARD_CHAR))) {
                // 屏蔽说明：不需要在这里过滤。
                else {
                    tmpList.splice(tmpList.indexOf(parseInt(key)), 1);
                    tmpList.splice(tmpList.indexOf(parseInt(key)), 1);
                    // 初选
                    console.log("tmpList:", tmpList);
                    const tmpCardTypeMap = this._GetCardTypeMap(tmpList);
                    console.log("==========================");
                    console.log(tmpCardTypeMap);
                    let tmpCardTypeList = _.filter(tmpCardTypeMap, function (o) { return (o.length % 3) != 0; });
                    if (tmpCardTypeList.length != 1) {
                        continue;
                    }
                    console.log("==========初测通过==========");

                }
            }
        }

        // 去重
        return _.uniq(winList);
    };

    private static _GetCardTypeMap(cardList: number[]) {
        return _.groupBy(cardList, function (o) {
            if ((o & CARDTYPE.CARD_NUMB) == CARDTYPE.CARD_NUMB) {
                return o & 0xFFF0;  // 屏蔽序号（万、索、饼归类）
            } else {
                return o & 0xFFFF;  // 无屏蔽
            }
        });
    };
};


