export const enum CARDTYPE {
    CARD_NUMB = 0x0100,  // 序数牌
    CARD_CHAR = 0x0200,  // 字牌
    CARD_FLOW = 0x0300,  // 花牌
    // const CARD_WILD = 0x0400,  // 百搭牌

    CARD_WILD = 0x1000,  // 癞子掩码(百搭牌)
    CARD_MUSK = 0xFFFF,  // 掩码

    TILE_WAN = 0x0010,  // 万
    TILE_SUO = 0x0020,  // 索
    TILE_BIN = 0x0030,  // 饼
    TILE_WIN = 0x0040,  // 风牌
    TILE_ARR = 0x0050,  // 箭牌
};

export const TYPE_WIN: number[] = [1, 4, 7, 10, 13, 16];
export const TYPE_READY: number[] = [2, 5, 8, 11, 14, 17];