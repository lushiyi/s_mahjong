

export const enum GAMEFLAG{
    GAME_DISCARD_WAIT = 0,  // 待出牌
    GAME_DISCARD_OK     ,   // 已出牌
    GAME_GANG_BU        ,   // 补杠
}
